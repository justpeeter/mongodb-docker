#!/bin/bash

mkdir containerdb

mongod --auth --dbpath=/containerdb &

until mongo admin --eval 'db.createUser({"user":"root", "pwd":"rootpwd", "roles": [ "root" ] })'
do
  echo 'Mongo not yet ready, waiting 1 sec until retry'
  sleep 1
done

mongo "mongodb://root:rootpwd@localhost:27017/admin" db.js

mongod --shutdown --dbpath=/containerdb
