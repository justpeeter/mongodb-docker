FROM mongo:3.2.21-jessie

COPY db.js /
COPY setup.sh /
RUN chmod +x /setup.sh
RUN /setup.sh

RUN rm -f setup.sh db.js

ENTRYPOINT ["/usr/bin/mongod"]
CMD ["--auth", "--dbpath=/containerdb"]
