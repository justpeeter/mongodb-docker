# build docker image MongoDB

1. Edit file db.js

 db = db.getSiblingDB('***ชื่อ database***'); <br>
db.createUser({"user":"***user***", "pwd":"***password***", "roles": [ { "role":"readWrite", "db":"***ชื่อ database***" } ] })

2. Build
``` console
$ docker build -t <image name> .
```

3. Run
``` console
$ docker run -dit -p 27017:27017 -n mongodb <image name>
```

4. Done!!
